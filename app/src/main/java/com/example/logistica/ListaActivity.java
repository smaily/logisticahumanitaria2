package com.example.logistica;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.logistica.Objetos.Device;
import com.example.logistica.Objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.DbEstados;
import database.Estados;

public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>,Response.ErrorListener{
    private DbEstados dbEstados;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private String serverip = "https://cursoandroidpatrick2019.000webhostapp.com/WebService/";
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> estados = new ArrayList<Estados>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);

        if(isNetworkAvailable()){
            request = Volley.newRequestQueue(this);
            this.mostrarTodosWebService(this);
        }
        else{
            dbEstados = new DbEstados(this);
            dbEstados.openDatabase();
            ArrayList<Estados> estados = dbEstados.allEstados();
            MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado, estados);
            setListAdapter(adapter);
        }

        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estados>{
        Context context;
        int textViewResourceId;
        DbEstados source = new DbEstados(ListaActivity.this);
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre =(TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombre());

            if(isNetworkAvailable()) {
                borrar.setEnabled(true);modificar.setEnabled(true);
            }else{
                borrar.setEnabled(false);modificar.setEnabled(false);
            }

            borrar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {

                    if(isNetworkAvailable()){
                        //Borra en el servidor externo
                        php.setContext(context);
                        Log.i("id",String.valueOf(objects.get(position).get_ID()));
                        php.borrarContactoWebService(objects.get(position).get_ID());

                        //Borra el sqlite
                        source.openDatabase();
                        source.borrarEstados(objects.get(position).get_ID());
                        source.close();
                        objects.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), "Estado eliminado con exito", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Se requiere conexión", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            modificar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    Bundle oBundle = new Bundle();

                    oBundle.putSerializable("estado", objects.get(position));

                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();

                }
            });
            return view;
        }
    }

    public void mostrarTodosWebService(Context context)
    {
        String url = serverip + "wsConsultarTodos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public boolean isNetworkAvailable() { //Función que verifica si hay conexión a internet
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {
        this.estados.removeAll(this.estados);
        Estados estado = null;
        JSONArray json = response.optJSONArray("estados");
        try {
            for (int i = 0; i < json.length(); i++) {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                estados.add(estado);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado,estados);
            setListAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

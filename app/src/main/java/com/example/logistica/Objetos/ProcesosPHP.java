package com.example.logistica.Objetos;
import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import database.Estados;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();
    private String serverip = "https://cursoandroidpatrick2019.000webhostapp.com/WebService/";
    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }

    public void insertarContactoWebService(Estados e){
        String url = serverip + "wsRegistro.php?nombre="+e.getNombre()+"&idMovil="+e.getIdMovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void actualizarContactoWebService(Estados e,int id){
        String url = serverip + "wsActualizar.php?_ID="+id+"&nombre="+e.getNombre()+"&idMovil="+e.getIdMovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void borrarContactoWebService(int id){
        String url = serverip + "wsEliminar.php?_ID="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public ArrayList<Estados> mostrarTodosWebService(Context context)
    {
        String url = serverip + "wsConsultarTodos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    public ArrayList<Estados> mostrarContactoWebService(Context context)
    {
        String url = serverip + "wsConsultarContacto.php?idMovil="+Device.getSecureId(context);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {

    }

}
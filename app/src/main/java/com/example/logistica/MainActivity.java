package com.example.logistica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.logistica.Objetos.Device;
import com.example.logistica.Objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.DbEstados;
import database.Estados;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,Response.Listener<JSONObject>,Response.ErrorListener {

    private EditText edtNombre;
    private Estados savedEstado;
    private boolean estadosAgregados;
    private int id;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private Button btnCerrar;
    ProcesosPHP php;
    private RequestQueue request;
    private String serverip = "https://cursoandroidpatrick2019.000webhostapp.com/WebService/";
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.php = new ProcesosPHP();

        request = Volley.newRequestQueue(this);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        this.php.setContext(this);

        SharedPreferences sharedPreferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        boolean agregados = sharedPreferences.getBoolean("agregados", false);

        if(!agregados){
            if(isNetworkAvailable()){
                this.mostrarTodosWebService(this);
                SharedPreferences.Editor editarPrimera = sharedPreferences.edit();
                editarPrimera.putBoolean("agregados", true);
                editarPrimera.commit();
            }
        }


        if(isNetworkAvailable()){
            btnGuardar.setEnabled(true);
        }
        else{
            btnGuardar.setEnabled(false);
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    boolean completo = true;
                    if (edtNombre.getText().toString().equals("")) {
                        edtNombre.setError("Introduce el nombre");
                        completo = false;
                    }

                    if (completo) {

                        DbEstados source = new DbEstados(MainActivity.this);
                        source.openDatabase();

                        Estados nEstado = new Estados();
                        nEstado.setNombre(edtNombre.getText().toString());
                        nEstado.setIdMovil(Device.getSecureId(MainActivity.this));


                        if (savedEstado == null) {
                            source.insertEstado(nEstado);
                            Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                            php.insertarContactoWebService(nEstado);
                            limpiar();
                        } else {
                            source.updateEstado(nEstado, id);
                            php.actualizarContactoWebService(nEstado, id);
                            Toast.makeText(MainActivity.this, R.string.mensajeEdit, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        source.close();
                    }
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(i, 0);
            }

        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
        } else {
            limpiar();
        }
    }

    public void mostrarTodosWebService(Context context)
    {
        String url = serverip + "wsConsultarTodos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {
        this.contactos.removeAll(this.contactos);
        Estados estado = null;
        JSONArray json = response.optJSONArray("estados");
        DbEstados bd = new DbEstados(this);
        bd.openDatabase();
        try {
            for (int i = 0; i < json.length(); i++) {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                bd.insertEstado(estado);
            }
            bd.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void limpiar() {
        savedEstado = null;
        edtNombre.setText("");
    }

    public boolean isNetworkAvailable() { //Función que verifica si hay conexión a internet
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    @Override
    public void onClick(View view) {
        if (isNetworkAvailable()) {
            switch (view.getId()) {
                case R.id.btnGuardar:
                    boolean completo = true;
                    if (edtNombre.getText().toString().equals("")) {
                        edtNombre.setError("Introduce el Nombre");
                        completo = false;
                    }
                    if (completo) {

                        DbEstados source = new DbEstados(MainActivity.this);
                        source.openDatabase();

                        Estados nEstado = new Estados();
                        nEstado.setNombre(edtNombre.getText().toString());


                        if (savedEstado == null) {
                            source.insertEstado(nEstado);

                            Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            php.actualizarContactoWebService(nEstado, id);
                            Toast.makeText(getApplicationContext(), R.string.mensajeEdit, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                    break;
                case R.id.btnLimpiar:
                    limpiar();
                    break;
                case R.id.btnListar:
                    Intent i = new Intent(MainActivity.this, ListaActivity.class);
                    limpiar();
                    startActivityForResult(i, 0);
                    break;
            }
        } else {
            Toast.makeText(getApplicationContext(), "Se necesita tener conexion a internet", Toast.LENGTH_SHORT).show();
        }
    }
}

